#!/usr/bin/python3
import netaddr, sys, argparse, subprocess, time, ipaddress
from os import path

parser = argparse.ArgumentParser(description='This tool takes a file with IP addresses or CIDR ranges as input and will try to compress the values into broader CIDRs to save space.')
parser.add_argument("input_file",type=str,help="File containing IP addresses")
parser.add_argument("format_type",type=str,help="The format of the processed IPs. Can be `CIDR` (ex: 192.168.0.0/24), `range` (192.168.0.0-255) or `individual` (list of all individual IPs)")
parser.add_argument("-o","--output",type=str,help="Output file name")
args = parser.parse_args()

IP_dict={}
sorted_binaryIPs={}

cidrs=[]
ranges=[]
individuals=[]

def load_and_sort(inputfile):
	with open(inputfile, "r") as lines:
		for item in lines:
			item=item.rstrip()
			if "/" in item:
				for IP in netaddr.IPNetwork(item):
					IP_dict[int(ipaddress.IPv4Address(IP))]=IP
			elif "-" in item:
				start,end = item.split("-")
				for IP_bin in range(int(ipaddress.IPv4Address(start)),int(ipaddress.IPv4Address(end))+1):
					IP_dict[IP_bin]=ipaddress.IPv4Address(IP_bin)
			else:
				IP_dict[int(ipaddress.IPv4Address(item))]=item
	return sorted(IP_dict.keys())

def parse_cidr():
	parse_range()
	for IPrange in ranges:
		start,end = IPrange.split("-")
		cidrs_split = netaddr.iprange_to_cidrs(start, end)
		for ipnetwork in cidrs_split:
			cidrs.append(str(ipnetwork))

def parse_range():
	set_base = "None"
	previous = "None"
	for key in sorted_binaryIPs:
		IP = str(IP_dict[key])
		if set_base=="None" and previous=="None":
			set_base=IP
			previous=set_base
			continue
		else:
			binary_previous=int(ipaddress.IPv4Address(previous))
			binary_IP=int(ipaddress.IPv4Address(IP))
			if binary_previous+1==binary_IP:
				previous=IP
				continue
			else:
				ranges.append(str(set_base)+"-"+str(previous))
				set_base=IP
				previous=IP
	ranges.append(str(set_base)+"-"+str(previous))
	set_base=IP
	previous=IP

def parse_individual():
	for key in sorted_binaryIPs:
		IP = str(IP_dict[key])
		individuals.append(IP)
		
def write_output(filename,output_item_list):
	with open(filename,'w') as output:
		for item in output_item_list:
			output.write(item)
			output.write("\n")


if path.exists(args.input_file):
	sorted_binaryIPs = load_and_sort(args.input_file)
	if args.format_type=="CIDR":
		parse_cidr()
		if args.output is not None:
			write_output(args.output,cidrs)
		else:
			for item in cidrs:
				print(item)
	elif args.format_type=="range":
		parse_range()
		if args.output is not None:
			write_output(args.output,ranges)
		else:
			for item in ranges:
				print(item)
	elif args.format_type=="individual":
		parse_individual()
		if args.output is not None:
			write_output(args.output,individuals)
		else:
			for item in individuals:
				print(item)
	else:
		print("[!] Unknown format_type. Select from `CIDR`,`range` or `individual`")
		sys.exit()
else:
	print("[!] Could not find input file, try again.")
	sys.exit()